#!/usr/bin/node

var service = require('./service');
var credentials = require('./credentials');
var monitoring = require('monitoring-lib');

var lastIp = null;

service.on('start', function(data) {
	console.log("Service started");
	lastIp = data.ip;
	monitoring.info({
		event: 'start',
		message: 'The service has just started, current ip: ' + data.ip,
		data: data
	});
});

service.on('stop', function(data) {
	console.log("Service stopped");
	monitoring.event({
		event: 'stop',
		message: 'The service is now stopped, last ip was: ' + data.ip,
		data: data
	});
	process.exit(0);
});

service.on('new-external-ip', function(data) {
	console.log("Got new ip !");
	lastIp = data.ip;
	monitoring.event({
		event: 'new-external-ip',
		message: 'The host have a new ip: ' + data.ip  + ', last was: ' + data.lastIp,
		data: data
	});
});

//Demande au serveur d'envoyer un message si la libmonitoring arrete de causer au serveur au bout de X secondes
monitoring.dead(function() {
	var message =  'Service is not sending heartbeat anymore, seems down ! Last ip was: ' + lastIp;
	return {
		pingInterval: 1,
		message: message
	};
});

//Lance la connection au monitoring externe
monitoring.start({
	//nom du service
	name: 			'external-ip-monitor',

	//serveur pour les pingbacks
	remoteapi: 		'localhost:3000',

	//report sur discord, slack, etc
	discord: 		credentials.discord

	//slack: {...}
}, function(err) {

	if(err) {
		console.log("Monitoring could not be started: " + err.message);
		return;
	}

	//monitoring started !
	console.log('Monitoring started');
});

process.on('SIGTERM', function () {
	console.log("catching sigterm");
	monitoring.stop();
    service.stop();
});

//Init du service avec les parametres
service.start({
	interval: 5
}, function(err) {

});