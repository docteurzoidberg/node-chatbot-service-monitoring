const getIP = require('external-ip')();
const EventEmitter = require('events');
const service = new EventEmitter();

service.lastIp = null;
service.intervalId = null;

service.checkExternalIp = function() {
    //console.log("Service checkIp tick");
    getIP((err, ip) => {
        if (err) {
            // every service in the list has failed 
            throw err;
        }

        console.log(ip);

        if(ip==service.lastIp) return;
    
        if(service.lastIp) {
            service.emit("external-ip-change", {lastIp: lastIp, ip: ip });            
        } else {
            service.emit("start", {ip: ip});
        }

        service.lastIp = ip;
    });
};

service.start = function(opts) {
    //console.log("Service checkIp start"); 
    var interval = opts.interval || 60;     
     service.checkExternalIp();
     service.intervalId = setInterval(service.checkExternalIp, interval*1000);
};

service.stop = function() {
    //console.log("Service checkIp stop"); 
    clearInterval(service.intervalId);
    service.emit("stop", {ip: service.lastIp});
};

module.exports = service;