const ClientApi = require('monitoring-httpclient');
const Discord = require('discord.js');

var api = null;
var discordWebhookClient = null;
var deadCalls = [];
var options = {};

function sendMessage(msg, cb) {

    if(!cb)
        cb=function(err) { if(err) throw err; };

    //Discord webhook client
    if(discordWebhookClient != null) {
        discordWebhookClient
            .sendMessage(msg)
            .then(function(){return cb(false);})
            .catch(function(err){return cb(err);});

        //let the client's callback return our callback
        return;
    }

    //Return error if no message sent
    return cb(new Error('no service was setup for sendMessage'));
}

process.on('SIGTERM', function () {
    console.log("Sigterm catched !");
    deadCalls.map(function(item) {
        api.clear();
        clearInterval(item.intervalId);
    });
});

var monitoring = {
    info:   function(data, cb)      {
        sendMessage(data.message, cb);
    },
    error:  function(data, cb)      {
        sendMessage(data.message, cb);
    },
    event:  function(data, cb)      {
        sendMessage(data.message, cb);
    },
    stop: function() {
        clear();
    },
    start:  function(opts, cb)      {

        //id = Math.floor(Math.random() * 100000000);
        options = opts;

        //Check if discord setup
        if(options.discord) {

            if(!options.discord.id) {
                return cb(new error('discord.id not set !'));
            }

            if(!options.discord.token) {
                return cb(new error('discord.token not set !'));
            }

            discordWebhookClient = new Discord.WebhookClient(options.discord.id, options.discord.token);
        }


        api = ClientApi(options.remoteapi, options.name, {discord: options.discord});

        //TODO: check server connectivity ?
        return cb(false);
    },

    dead:   function(fn)  {

        var item = fn();
        if(!item.pingInterval)
            throw new Error('Ping interval must be set');

        var intervalId = setInterval(function() {
            var item = fn();
            api.ping(item);
        }, item.pingInterval*1000);

        deadCalls.push({
            id: intervalId,
            item: item
        });
    }
};

module.exports = monitoring;
