const Request = require('request');

var Client = {};

Client.id = "";
Client.remote = "";
Client.service = "";
Client.credentials = {};

Client.ping = function(call, cb) {    

    cb = cb ? cb : function(err, response) {
        if(err) { 
            console.error(err); return; 
        }
        console.log("Get response: " + response.statusCode);
    };

    Request({url:"http://" + Client.remote + "/ping", qs: {
        id:             Client.id,       
        service:        Client.service,
        discordtoken:   Client.credentials.discord.token,
        discordid:      Client.credentials.discord.id,
        timeout:        call.pingInterval*2,
        message:        call.message
    }}, cb);
}

Client.clear = function(cb) {
    
    cb = cb ? cb : function(err, response) {
        if(err) { 
            console.error(err); return; 
        }
        console.log("Get response: " + response.statusCode);
    };

    Request({url:"http://" + Client.remote + "/clear", qs: {
        id: Client.id
    }}, cb);
}

module.exports = function(remote, service, credentials) {
    Client.id = Math.floor(Math.random() * 100000000);
    Client.remote = remote;
    Client.service = service;
    Client.credentials = credentials;
    return Client;
} 
